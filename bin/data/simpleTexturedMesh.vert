#version 150

out vec2 v_texcoord;

in vec4 position;
in vec4 color;
in vec4 normal;
in vec2 texcoord;

// These are passed in from OF programmable renderer
uniform mat4 modelViewMatrix;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 textureMatrix;
uniform mat4 modelViewProjectionMatrix;

void main (void){
    v_texcoord = texcoord;
    gl_Position = modelViewProjectionMatrix * position;
}
