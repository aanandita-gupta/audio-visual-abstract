#pragma once

#include "ofMain.h"
#include "ofxMaxim.h"
#include "maxiGrains.h"
#include "ofxOsc.h"

#define HOST "localhost"
#define RECEIVEPORT 12000
#define SENDPORT 6448

typedef hannWinFunctor grainPlayerWin;
using namespace glm;

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();    
    
		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
    
		ofEasyCam myCamera;
		ofShader surfaceShader;
		ofBoxPrimitive box;
        
        /* audio stuff */
        void audioOut(ofSoundBuffer& output) override; //output method
        void audioIn(ofSoundBuffer& input) override; //input method
        ofSoundStream soundStream;
        
        /* stick your maximilian stuff below */
        //this is from the maxigranualr example
        double wave, outputs[2];
        maxiSample samp, samp2, samp3, samp4, samp5;
        vector<maxiTimePitchStretch<grainPlayerWin, maxiSample>*> samples;
        maxiMix mymix;
        double speed, grainLength;

        ofxMaxiFFT featureextractor;
        ofxMaxiFFTOctaveAnalyzer octaveanalyser;
        int current;
        double pos;
        
        //using a compressor to compress the file.
        //I have used the compressor example given to learn and experiment more with it.
        maxiDyn compressor; //this is a compressor
        maxiOsc timer, mySine, myOtherSine, myOtherOtherSine; //and a timer
        
        //These are the synthesiser bits
        maxiOsc VCO1,VCO2,LFO1;
        maxiFilter VCF;
        maxiEnv ADSR;
        double VCO1out,VCO2out,LFO1out,LFO2out,VCFout,ADSRout;
        //oscillators for calculaitng the low pass filter
        maxiOsc myOsc, myLF01, myLF02, myPhasor;
        maxiFilter myPortamento;
        double oscOutput, filterOut, ampOut, LF01Output, LF02Output, portaOut;
        //sample hits
        double hit[16]={0,0,0.5,0,0.68,0,0.2,0,0.5,0,0,0,0,0,0,0};
        double snaretrigger;
        int playHead;
        
        
        //for the cloud
        float time0 = 0;
        float Rad = 50;        //Cloud raduis parameter
        float Vel = 0.1;        //Cloud points velocity parameter
        int bandRad = 2;        //Band index in spectrum, affecting Rad value
        int bandVel = 50;        //Band index in spectrum, affecting Vel value
    
};
